# Character Animation Converter

## What is this tool for?

CharAnimConvert is a tool that was created for an used in the production of the REHARDEN demo for Atari 8-bit computers (64K PAL XL/XE).
See the rotating/falling "READY" screen or the Twister-Scroller (greetings) part.
You could extend it with custom animation providers to suit your needs.

## What does this repo include?

This repo includes the full source code of CharAnimConvert in the form of an Eclipse Neon project.

There's no separate documentation written for this tool, please see the source code for examples on usage (main function).

Sandor Teli / HARD
