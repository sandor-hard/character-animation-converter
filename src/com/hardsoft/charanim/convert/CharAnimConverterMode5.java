package com.hardsoft.charanim.convert;

public class CharAnimConverterMode5 extends AbstractCharAnimConverter {

	@Override
	protected int getCharHeight() {
		return 16;
	}

	@Override
	protected int getCharWidth() {
		return 8;
	}

	@Override
	protected int getBpp() {
		return 2;
	}

	@Override
	protected int getCharsPerCharset() {
		return 128;
	}
	
	@Override
	protected boolean getSupportsInverseChars() {
		return false;
	}
	
}
