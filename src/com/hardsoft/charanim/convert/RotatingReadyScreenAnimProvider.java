/*
    Rotating READY Screen animation generator
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to modify/use whatever way you like
 */

package com.hardsoft.charanim.convert;

import java.util.HashMap;
import java.util.Map;

import com.hardsoft.charanim.convert.CharAnimConverter.AnimProvider;
import com.hardsoft.charanim.convert.CharAnimConverter.RawAnim;
import com.hardsoft.charanim.convert.CharAnimConverter.Palette;

public class RotatingReadyScreenAnimProvider implements AnimProvider {

	private static final int FRAME_COUNT = 128;
	private static final double SWINGS_CNT = 2.8d;
	private static final float DAMPENING_FACTOR = 0.6f; //smaller->slower/heavier object
	private static final int DEGREES_SUPPORTED = 192;
	
	private static final int OUT_RES_X = 42*8;
	private static final int OUT_RES_Y = 30*8;
	private static final int UPSCALING_FACTOR = 2;

	private static final int TOTAL_CHARS_X = OUT_RES_X>>3;
	private static final int TOTAL_CHARS_Y = OUT_RES_Y>>3;
	private static final int BORDER_X = (TOTAL_CHARS_X - 40)<<2;
	private static final int BORDER_Y = (TOTAL_CHARS_Y - 24)<<2;
	
	private static final int IN_RES_X = OUT_RES_X*UPSCALING_FACTOR;
	private static final int IN_RES_Y = OUT_RES_Y*UPSCALING_FACTOR;
	private static final int IN_CHAR_SIZE = 8*UPSCALING_FACTOR;
	private static final int IN_BORDER_X = BORDER_X*UPSCALING_FACTOR;
	private static final int IN_BORDER_Y = BORDER_Y*UPSCALING_FACTOR;
	private static final int IN_CURSOR_X = (BORDER_X+16)*UPSCALING_FACTOR;
	private static final int IN_CURSOR_Y = BORDER_Y*UPSCALING_FACTOR;
	
	private static final int COLBK = 0; //border color in hi-res
	private static final int COLPF0 = 0x08; //not used in hi-res, serves as transition color between PF1 & PF2
	private static final int COLPF1 = 0x0a; //foreground luminance in hi-res
	private static final int COLPF2 = 4; //background color in hi-res
	private static final int COLPF3 = 0; //not used in hi-res
	
	private static final double DEGREES_SUPPORTED_FACTOR = 2f*Math.PI/(double)DEGREES_SUPPORTED;
	
	private final int[][] mReadyScreen = new int[IN_RES_X][IN_RES_Y];
	
	@Override
	public RawAnim getRawAnim(String uri) {
		final RawAnim result = new RawAnim(new Palette(COLBK, COLPF0, COLPF1, COLPF2, COLPF3), FRAME_COUNT);
		
		createReadyScreen();

		final double cosStep = (double) SWINGS_CNT * 2f*Math.PI/(double) FRAME_COUNT;
		
		final Map<Double, Integer> degreeFrameIndexMap = new HashMap<Double, Integer>();
		
		for (int f=0;f<FRAME_COUNT;f++) {
			final double dampen = 1f - Math.pow(1f / (double)FRAME_COUNT * f, DAMPENING_FACTOR);
			final double progress = cosStep * (double) f;
			final double swingAmount = (1f - Math.cos(progress))/2f * Math.PI;
			final double rotation = roundToSupportedDegree((swingAmount - Math.PI/2f)*dampen+Math.PI/2f);
			
			if (degreeFrameIndexMap.containsKey(rotation)) {
				result.frameIndices[f] = degreeFrameIndexMap.get(rotation);
			}
			else {
				final int[][] frame = new int[IN_RES_X][IN_RES_Y];
				for (int x=0;x<IN_RES_X;x++) {
					for (int y=0;y<IN_RES_Y;y++) {
						final int c = getRotatedColor(rotation, x, y);
						frame[x][y] = c;
					}
				}
				
				final int[][] outFrame = new int[OUT_RES_X][OUT_RES_Y];
				for (int x=0;x<OUT_RES_X;x++) {
					for (int y=0;y<OUT_RES_Y;y++) {
						final int c = frame[x*UPSCALING_FACTOR][y*UPSCALING_FACTOR];
						outFrame[x][y] = c;
					}
				}
				
				result.frameIndices[f] = result.frames.size();
				degreeFrameIndexMap.put(rotation, result.frames.size());
				result.frames.add(outFrame);
			}
		}
		
		return result;
	}

	private double roundToSupportedDegree(double d) {
		return Math.round(d / DEGREES_SUPPORTED_FACTOR) * DEGREES_SUPPORTED_FACTOR;
	}

	private int getRotatedColor(double rotation, int x, int y) {
		final double cos = Math.cos(2f*Math.PI-rotation);
		final double sin = Math.sin(2f*Math.PI-rotation);
		
		final int offsX = IN_CURSOR_X + IN_CHAR_SIZE/2;
		final int offsY = IN_CURSOR_Y + IN_CHAR_SIZE/2;
		
		int tX = x - offsX;
		int tY = y - offsY;
		
		final int rX = (int) Math.round(cos * tX - sin * tY + offsX);
		final int rY = (int) Math.round(sin * tX + cos * tY + offsY);
		
		final boolean isInvalid = (rX<0 || rY<0 || rX>=IN_RES_X || rY>=IN_RES_Y);
		final int result = isInvalid?Palette.COLOR_INDEX_BK:mReadyScreen[rX][rY];
		
		return result;
	}

	private void createReadyScreen() {
		for (int x=0;x<IN_RES_X;x++) {
			for (int y=0;y<IN_RES_Y;y++) {
				final boolean isBorder = (x<IN_BORDER_X) || (x>=IN_RES_X-IN_BORDER_X) ||
						(y<IN_BORDER_Y) || (y>=IN_RES_Y-IN_BORDER_Y);
				final boolean isCursor = false;//(!isBorder)&&(x>=IN_CURSOR_X)&&(x<(IN_CURSOR_X+IN_CHAR_SIZE))
						//&&(y>=IN_CURSOR_Y)&&(y<(IN_CURSOR_Y+IN_CHAR_SIZE));
				final int c = isBorder?Palette.COLOR_INDEX_BK:(isCursor?Palette.COLOR_INDEX_PF1:Palette.COLOR_INDEX_PF2);
				mReadyScreen[x][y] = c;
			}
		}
	}

}
