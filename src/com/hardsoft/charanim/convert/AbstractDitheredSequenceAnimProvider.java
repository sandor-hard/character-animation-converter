/*
    Character animation converter
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to modify/use whatever way you like
 */

package com.hardsoft.charanim.convert;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.marvinproject.image.color.skinColorDetection.ColorSpaceConverter;

import com.hardsoft.charanim.convert.CharAnimConverter.AnimProvider;
import com.hardsoft.charanim.convert.CharAnimConverter.Palette;
import com.hardsoft.charanim.convert.CharAnimConverter.RawAnim;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

public abstract class AbstractDitheredSequenceAnimProvider implements AnimProvider {

	final static double IGNORE_DITHERING_BELOW_BRIGHTNESS = 0.1f;
	final static double IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE = 0.0000001f;
	final static double RESPECT_BRIGHTNESS_ONLY_RATIO = 1f; //1f = Pseudo-black&white; 0.99f-0.95f = colors+brightness
	final static int DITHER_MATRIX_DIMENSION_POW = 3;
	final static double MAX_FROM_TO_DISTANCE_TO_DITHER = 0.33f;
	final static boolean ALLOW_BLACK_DARKEST_DITHERING = false;
	
	final static String PALETTE_PATH = "palette/altirra.pal";
	
	class AnimPalette {
		final int index;

		final int[] atariColors;
		final int[] baseRgbColors;
		final int[] rgbColors;
		final int[][] hsvColors;
		final double[] brightness;
		
		final int ditherSteps;
		final int ditherArrayDimension;
		final int[][] ditherMap;
		
		final int[] baseColorFrom;
		final int[] baseColorTo;
		final double[] fromToDist;
		final double[] transitionProgress;
		final int[] ditherStep;
		
		int blackToDarkestFrom = -1;
		int blackToDarkestTo = -1;
		
		final int cnt;
		
		int ditherMapCnt = 0;
		
		private AnimPalette(int index, byte[] paletteFileData, int[] atariColors,
				int ditherMatrixDimensionPow, double respectBrightnessOnlyRatio) {
			final int baseCnt = atariColors.length;
			
			this.index = index;
			
			this.atariColors = atariColors;
			this.baseRgbColors = new int[baseCnt];
			
			this.ditherArrayDimension = (int) Math.pow(2, ditherMatrixDimensionPow);
			this.ditherSteps = ditherArrayDimension*ditherArrayDimension - 1; //steps without fromRgb/empty & toRgb/full
			this.ditherMap = new int[ditherArrayDimension][ditherArrayDimension];
			
			//Example for 5 baseCnt: 4+3+2+1= 10 connections
			//10 connections * ditherSteps + baseColorsRecordedOnce
			int cntTmp = baseCnt;
			for (int bC=0;bC<(baseCnt-1);bC++) {
				cntTmp+= (bC+1)*ditherSteps;
			}
			cnt = cntTmp;

			System.out.println(String.format("  Adding palette: Working with %d combinations of %d base colors (%s dither steps)", cnt, baseCnt, ditherSteps));
			System.out.print("    Base colors: ");

			for (int bC=0;bC<baseCnt;bC++) {
				baseRgbColors[bC] = atariToRgb(paletteFileData, atariColors[bC]);
				
				System.out.print("$" + Integer.toHexString(baseRgbColors[bC]) + " ");
			}
			System.out.println();
			
			rgbColors = new int[cnt];
			baseColorFrom = new int[cnt];
			baseColorTo = new int[cnt];
			fromToDist = new double[cnt];
			transitionProgress = new double[cnt];
			ditherStep = new int[cnt];
			
			int cix = 0;
			
			//Recording base colors first
			for (int bC=0;bC<baseCnt;bC++) {
				final int rgb = baseRgbColors[bC];

				//Record base color (from=to; progress=100%)
				baseColorFrom[cix] = bC;
				baseColorTo[cix] = bC;
				transitionProgress[cix] = 1f;
				ditherStep[cix] = 0;
				rgbColors[cix++] = rgb;
			}
			
			//Creating all connections/transitions between base colors
			for (int fromC=0;fromC<(baseCnt-1);fromC++) {
				for (int toC=fromC+1;toC<baseCnt;toC++) {
					final int fromRgb = baseRgbColors[fromC];
					final int toRgb = baseRgbColors[toC];
					
					final int fromR = (fromRgb>>16)&0xff;
					final int fromG = (fromRgb>>8)&0xff;
					final int fromB = fromRgb&0xff;
					
					final int toR = (toRgb>>16)&0xff;
					final int toG = (toRgb>>8)&0xff;
					final int toB = toRgb&0xff;
	
					final double diffR = toR - fromR;
					final double diffG = toG - fromG;
					final double diffB = toB - fromB;
					
					for (int d=0;d<ditherSteps;d++) {
						final double progress = 1f/(double)(ditherSteps+1)*(float)(d+1);
						final int r = (int) ((double)fromR + (diffR*progress));
						final int g = (int) ((double)fromG + (diffG*progress));
						final int b = (int) ((double)fromB + (diffB*progress));
						final int rgb = r<<16|g<<8|b;
	
						baseColorFrom[cix] = fromC;
						baseColorTo[cix] = toC;
						transitionProgress[cix] = progress;
						ditherStep[cix] = d;
						rgbColors[cix++] = rgb;
					}
					
//					System.out.println(String.format("Added transitions between: fromC=%d, toC=%d (cix=%d)", fromC, toC, cix));
				}
			}

//			System.out.println();
			
			this.hsvColors = new int[cnt][3];
			this.brightness = new double[cnt];
			for (int c=0;c<cnt;c++) {
				final int rgb = rgbColors[c];

				final int rByte = (rgb>>16)&0xff;
				final int gByte = (rgb>>8)&0xff;
				final int bByte = rgb&0xff;
				
				final float[] hsvFloat = Color.RGBtoHSB(rByte, gByte, bByte, null);
				hsvColors[c][0] = Math.round(255f * hsvFloat[0]);
				hsvColors[c][1] = Math.round(255f * hsvFloat[1]);
				hsvColors[c][2] = Math.round(255f * hsvFloat[2]);
				
				final double br = calculateBrightness(rByte, gByte, bByte);
				brightness[c] = br;
			}
			
			double blackToDarkestDist = Double.MAX_VALUE;
			
			for (int c=0;c<cnt;c++) {
				if (baseColorFrom[c]==baseColorTo[c]) {
					fromToDist[c] = 0f;
				}
				else {
					final int rgbFrom = baseRgbColors[baseColorFrom[c]];
					final int rgbTo = baseRgbColors[baseColorTo[c]];
					
					final int rByteFrom = (rgbFrom>>16)&0xff;
					final int gByteFrom = (rgbFrom>>8)&0xff;
					final int bByteFrom = rgbFrom&0xff;

					final int rByteTo = (rgbTo>>16)&0xff;
					final int gByteTo = (rgbTo>>8)&0xff;
					final int bByteTo = rgbTo&0xff;
					
					final float[] hsvFloatFrom = Color.RGBtoHSB(rByteFrom, gByteFrom, bByteFrom, null);
					final int fromH = Math.round(255f * hsvFloatFrom[0]);
					final int fromS = Math.round(255f * hsvFloatFrom[1]);
					final int fromV = Math.round(255f * hsvFloatFrom[2]);
					
					final float[] hsvFloatTo = Color.RGBtoHSB(rByteTo, gByteTo, bByteTo, null);
					final int toH = Math.round(255f * hsvFloatTo[0]);
					final int toS = Math.round(255f * hsvFloatTo[1]);
					final int toV = Math.round(255f * hsvFloatTo[2]);
					
					final double brightnessFrom = calculateBrightness(rByteFrom, gByteFrom, bByteFrom);
					final double brightnessTo = calculateBrightness(rByteTo, gByteTo, bByteTo);
					final double distBrightness = Math.abs(brightnessFrom-brightnessTo);
					final double distHsv = getHsvDistance(fromH, fromS, fromV, toH, toS, toV);
					fromToDist[c] = Math.sqrt(respectBrightnessOnlyRatio*distBrightness*distBrightness + (1f-respectBrightnessOnlyRatio)*distHsv*distHsv);
					
					if (brightnessFrom==0.0d && fromToDist[c]<blackToDarkestDist) {
						blackToDarkestDist = fromToDist[c];
						blackToDarkestFrom = baseColorFrom[c];
						blackToDarkestTo = baseColorTo[c];
					}
				}
			}
			
			generateDithermap();
		}

		private void generateDithermap() {
			for (int[] row : ditherMap) {
				Arrays.fill(row, -1);
			}
			
			ditherMapCnt = 0;

			final DitherMapSection ds = getDitherMapSection(0, 0, ditherArrayDimension);
			while (!ds.isDone()) {
				step(ds);
			}

//			System.out.println();
//			
//			for (int y=0;y<ditherArrayDimension;y++) {
//				if (y>0 && (y%4==0)) {
//					for (int sep=0;sep<ditherArrayDimension*4+1;sep++) {
//						System.out.print("-");
//					}
//					System.out.println();
//				}
//				for (int x=0;x<ditherArrayDimension;x++) {
//					if (x>0 && (x%4==0)) {
//						System.out.print("|");
//					}
//					final int v = ditherMap[x][y];
//					System.out.print(v==-1?"   .":String.format("%4s", v));
//				}
//				System.out.println();
//			}
		}

		private DitherMapSection getDitherMapSection(int x, int y, int dim) {
			DitherMapSection[] children = null;
			
			if (dim>=4) {
				final int childDim = dim/2;
				final DitherMapSection c1 = getDitherMapSection(x, y+childDim, childDim);
				final DitherMapSection c2 = getDitherMapSection(x+childDim, y, childDim);
				final DitherMapSection c3 = getDitherMapSection(x, y, childDim);
				final DitherMapSection c4 = getDitherMapSection(x+childDim, y+childDim, childDim);
				children = new DitherMapSection[]{c1, c2, c3, c4};
			}

			return new DitherMapSection(x, y, dim, children);
		}

		private void step(DitherMapSection ds) {
			if (!ds.isDone()) {
				if (ds.dim>2) {
					step(ds.children[ds.stepsDone]);
					ds.stepsDone = (ds.stepsDone+1)&3;
				}
				else {
					final int xInc = (ds.stepsDone==1 || ds.stepsDone==3)?1:0;
					final int yInc = (ds.stepsDone==0 || ds.stepsDone==3)?1:0;
					ditherMap[ds.x+xInc][ds.y+yInc] = ditherMapCnt++;
					ds.stepsDone++;
				}
			}
		}
		
		class DitherMapSection {
			final int x;
			final int y;
			final int dim;
			final DitherMapSection[] children;
			
			int stepsDone = 0;
			
			public DitherMapSection(int x, int y, int dim, DitherMapSection[] children) {
				this.x = x;
				this.y = y;
				this.dim = dim;
				this.children = children;
			}
			
			boolean isDone() {
				boolean result = false;
				
				if (dim>2) {
					int doneCnt = 0;
					for (DitherMapSection child : children) {
						if (child.isDone()) {
							doneCnt++;
						}
					}
					result = doneCnt==children.length;
				}
				else {
					result = stepsDone==4;
				}
				
				return result;
			}
		}
	}

	protected static final int OUT_RES_X = 42*8;
	protected static final int OUT_RES_Y = 30*8;

	private static final int COLBK = 0;
	private static final int COLPF0 = 0x04;
	private static final int COLPF1 = 0x08;
	private static final int COLPF2 = 0x0c;
	
	private static final int PIXELS_PER_BYTE = 4;
	private static final int BITS_PER_PIXEL = 8/PIXELS_PER_BYTE;
	
	private byte[] paletteFileData; //not used

	AnimPalette[] palettes;
	
	@Override
	public RawAnim getRawAnim(String uri) {

		initPalette();
		
		final ArrayList<int[][]> frameList = new ArrayList<int[][]>();
		
		int frameCount = 0;
        while (true) {
        	try {
        		final MarvinImage image = generateFrameImage(uri, frameCount);

        		if (image!=null) {
        			final MarvinImage imageIn = image.clone();
        			final MarvinImage imageOut = image;
        			
        			final int srcW = imageIn.getWidth();
        			final int srcH = imageIn.getHeight();
        			
        			final float scaleFactor = getScaleFactor(OUT_RES_X, OUT_RES_Y, srcW, srcH);
        			
        			final int newW = (int) (srcW * scaleFactor);
        			final int newH = (int) (srcH * scaleFactor);
        			
        			final int paddingTop = (OUT_RES_Y - newH) / 2;
        			final int paddingLeft = (OUT_RES_X - newW) / 2;
        			
        			imageOut.resize(OUT_RES_X, OUT_RES_Y);
        			
        			drawDownscaledImage(imageIn, imageOut, OUT_RES_X, OUT_RES_Y, 1f, scaleFactor, paddingTop, paddingLeft);

        			final ColorSpaceConverter colorspaceConverter = new ColorSpaceConverter();
        			final MarvinImage hsvImage = new MarvinImage(OUT_RES_X, OUT_RES_Y);
        			colorspaceConverter.process(imageOut, hsvImage, null, null, false);
        			
        			final int[][] frame = new int[OUT_RES_X][OUT_RES_Y];
        			
        			for (int y = 0; y < OUT_RES_Y; y++) {
        				for (int x = 0; x < OUT_RES_X; x+=8) {
        					
            				final double[] paletteMinDistForPixel = new double[PIXELS_PER_BYTE];
            				final int[] paletteMinDistForPixelColorIndex = new int[PIXELS_PER_BYTE];
            				
                			final AnimPalette wp = palettes[0];

                			for (int xOffs=0;xOffs<8;xOffs+=BITS_PER_PIXEL) {
            					//0..255 => 0..360
            					final int iH = hsvImage.getIntComponent0(x+xOffs, y);
            					//0..255 => 0..1f
            					final int iS = hsvImage.getIntComponent1(x+xOffs, y);
            					//0..255 => 0..1f
            					final int iV = hsvImage.getIntComponent2(x+xOffs, y);
            					
        						paletteMinDistForPixel[xOffs/BITS_PER_PIXEL] = Double.MAX_VALUE;
        						paletteMinDistForPixelColorIndex[xOffs/BITS_PER_PIXEL] = 0;
        						
        						calcMinDistancesForPalette(imageOut, ALLOW_BLACK_DARKEST_DITHERING, RESPECT_BRIGHTNESS_ONLY_RATIO,
        								MAX_FROM_TO_DISTANCE_TO_DITHER, y, x, paletteMinDistForPixel,
        								paletteMinDistForPixelColorIndex, xOffs, iH, iS, iV, 0, wp, false);
            				}
            				
            				for (int px=0;px<PIXELS_PER_BYTE;px++) {
        						final int colorIndex = paletteMinDistForPixelColorIndex[px];
        						final boolean isDithered = wp.baseColorFrom[colorIndex]!=wp.baseColorTo[colorIndex];
        						int resultBaseColorIndex;
        						
    							if (isDithered) {
    								final double tresholdSetting = wp.brightness[colorIndex]<IGNORE_DITHERING_BELOW_BRIGHTNESS?
    										1f:IGNORE_DITHERING_ABOVE_OR_BELOW_DISTANCE_PERCENTAGE;
    								final double treshold = tresholdSetting * wp.fromToDist[colorIndex];
    								final boolean isBelowTreshold = wp.transitionProgress[colorIndex]<treshold;
    								final boolean isAboveTreshold = wp.transitionProgress[colorIndex]>(1f-treshold);
    								if (!isBelowTreshold && !isAboveTreshold) {
    									final int ditherStep = wp.ditherStep[colorIndex];
    									
    									final int ditherMapValue = 
    											wp.ditherMap[(x + px)&(wp.ditherArrayDimension-1)][y&(wp.ditherArrayDimension-1)];
    									final boolean doesJump = ditherStep>=ditherMapValue;
    									resultBaseColorIndex = doesJump?wp.baseColorTo[colorIndex]:wp.baseColorFrom[colorIndex];
    								}
    								else {
    									if (isBelowTreshold) {
    										resultBaseColorIndex = wp.baseColorFrom[colorIndex];
    									}
    									else {
    										resultBaseColorIndex = wp.baseColorTo[colorIndex];
    									}
    								}
    							}
    							else {
    								resultBaseColorIndex = wp.baseColorFrom[colorIndex];
    							}

        						final int rgb = wp.baseRgbColors[resultBaseColorIndex];
        						for (int subPx=0;subPx<BITS_PER_PIXEL;subPx++) {
        							imageOut.setIntColor(x + px*BITS_PER_PIXEL+subPx, y, (255<<24)+rgb);
        							frame[x + px*BITS_PER_PIXEL+subPx][y] = resultBaseColorIndex;
        						}
        					}
        				}
        			}
        			
        			frameList.add(frame);
        			
            		imageOut.update(); 
            		final String outFileName = String.format("inout/frame%d.out.%s", frameCount, imageOut.getFormatName());
            		new File(outFileName).delete();
					MarvinImageIO.saveImage(imageOut, outFileName);
        			
        			frameCount++;
        		}
        		else {
        			break;
        		}
        		
			} catch (Exception e) {
				break;
			}
        }
                
		final RawAnim result = new RawAnim(new Palette(COLBK, COLPF0, COLPF1, COLPF2, 0), frameCount);

		for (int f=0;f<frameCount;f++) {
			result.frameIndices[f] = result.frames.size();
			result.frames.add(frameList.get(f));
		}

		return result;
	}

	protected abstract MarvinImage generateFrameImage(String uri, int frame);

	protected void drawDownscaledImage(MarvinImage imageIn, MarvinImage imageOut, final int w, final int h,
			final double gamma, final float scaleFactor, final int paddingTop, final int paddingLeft) {
		for (int y = 0; y < h; y++) {				
			for (int x = 0; x < w; x+=2) {
				final int yUnits = (int) (Math.ceil(1.0f/scaleFactor));
				final int l_rgb = getPredominantRGB(imageIn, (int) ((x-paddingLeft)/scaleFactor),
						(int) ((y-paddingTop)/scaleFactor), yUnits*2, yUnits, gamma);
				imageOut.setIntColor(x, y, (255<<24)+l_rgb);
				imageOut.setIntColor(x+1, y, (255<<24)+l_rgb);
			}
		}
	}
	
	protected int getPredominantRGB(MarvinImage a_image, int a_x, int a_y, int xUnits, int yUnits,
			double gamma){
		int l_red=-1;
		int l_green=-1;
		int l_blue=-1;
		
		for(int x=a_x; x<a_x+xUnits; x++){
			for(int y=a_y; y<a_y+yUnits; y++){
				if(x < a_image.getWidth() && y < a_image.getHeight()){
					if(l_red == -1)		l_red = a_image.getIntComponent0(x,y);
					else				l_red = (l_red+a_image.getIntComponent0(x,y))/2;
					if(l_green == -1)	l_green = a_image.getIntComponent1(x,y);
					else				l_green = (l_green+a_image.getIntComponent1(x,y))/2;
					if(l_blue == -1)	l_blue = a_image.getIntComponent2(x,y);
					else				l_blue = (l_blue+a_image.getIntComponent2(x,y))/2;	
				}				
			} 
		}
		return (l_red<<16)+(l_green<<8)+l_blue;
	}

	private float getScaleFactor(float destW, float destH, float srcW, float srcH) {
	    float destAspect = destW / destH;
	    float srcAspect = srcW / srcH;

	    float scaleFactor;
	    if (destAspect < srcAspect) {
	        scaleFactor = destH / srcH;
	    }
	    else {
	        scaleFactor = destW / srcW;
	    }

	    return scaleFactor;
	}	
	
	protected int atariToRgb(byte[] paletteFileData, int c) {
		final int r = byteToUnsignedInt(paletteFileData[c*3]);
		final int g = byteToUnsignedInt(paletteFileData[c*3+1]);
		final int b = byteToUnsignedInt(paletteFileData[c*3+2]);
		return (r<<16)|(g<<8)|b;
	}
	
	protected int byteToUnsignedInt(byte b) {
		return b>=0?(int)b:((int)b)+0x100;
	}
	
	protected double calculateBrightness(final int rByte, final int gByte, final int bByte) {
		final double r = rByte/255f;
		final double g = gByte/255f;
		final double b = bByte/255f;
		
		final double br = Math.sqrt(0.299f * r*r + 0.587f * g*g + 0.114 * b*b);
		return br;
	}

	private double getHsvDistance(int h0, int s0, int v0, int h1, int s1, int v1) {
		final float hc0 = h0 / 255f * 360f;
		final float hc1 = h1 / 255f * 360f;
		
		final float dh = Math.min(Math.abs(hc1-hc0), 360-Math.abs(hc1-hc0)) / 180.0f;
		
		final float	ds = Math.abs(s1-s0) / 255f;
		final float dv = Math.abs(v1-v0) / 255f;
		
		final double result = Math.sqrt(dh*dh+ds*ds+dv*dv);
		
		return result;
	}
	
	void initPalette() {
		paletteFileData = new byte[0x300];
		FileInputStream f = null;
		try {
			f = new FileInputStream(PALETTE_PATH);
			int bytesRead = 0;
			int totalBytesRead = 0;
			while ((bytesRead=f.read(paletteFileData, totalBytesRead, paletteFileData.length-totalBytesRead))>0) {
				totalBytesRead+= bytesRead;
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not load palette file");
		} finally {
			if (f!=null) {
				try {
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		palettes = new AnimPalette[]{
				new AnimPalette(0, paletteFileData, new int[]{
						COLBK,
						COLPF0,
						COLPF1,
						COLPF2},
						DITHER_MATRIX_DIMENSION_POW,
						RESPECT_BRIGHTNESS_ONLY_RATIO)};
	}

	private void calcMinDistancesForPalette(MarvinImage imageOut, final boolean allowBlackDarkestDithering,
			final double respectBrightnessOnlyRatio,
			final double maxFromToDistanceToDither, int y, int x,
			final double[] paletteMinDistForPixel, final int[] paletteMinDistForPixelColorIndex, int xOffs,
			final int iH, final int iS, final int iV, int p, final AnimPalette palette, final boolean baseColorOnly) {
		for (int c=0;c<palette.cnt;c++) {
			final boolean isDithered = palette.baseColorFrom[c]!=palette.baseColorTo[c];

			if (!isDithered || !baseColorOnly) {
				final int pH = palette.hsvColors[c][0];
				final int pS = palette.hsvColors[c][1];
				final int pV = palette.hsvColors[c][2];
				
				//distance for image pixel vs palette color
				final double distHsv = getHsvDistance(iH, iS, iV, pH, pS, pV);
				
				final int r = imageOut.getIntComponent0(x+xOffs, y);
				final int g = imageOut.getIntComponent1(x+xOffs, y);
				final int b = imageOut.getIntComponent2(x+xOffs, y);
				final double brightness = calculateBrightness(r, g, b);
				
				final double distBrightness = Math.abs(palette.brightness[c] - brightness);
				
				final boolean isBlackToDarkestException = palette.baseColorFrom[c]==palette.blackToDarkestFrom &&
						palette.baseColorTo[c]==palette.blackToDarkestTo && allowBlackDarkestDithering;

				if (!isDithered || isBlackToDarkestException || palette.fromToDist[c]<=maxFromToDistanceToDither) {
					final double dist = Math.sqrt(respectBrightnessOnlyRatio*distBrightness*distBrightness
							+ (1f-respectBrightnessOnlyRatio)*distHsv*distHsv);
					
					if (dist<paletteMinDistForPixel[xOffs/BITS_PER_PIXEL]) {
						paletteMinDistForPixel[xOffs/BITS_PER_PIXEL] = dist;
						paletteMinDistForPixelColorIndex[xOffs/BITS_PER_PIXEL] = c;
					}
				}
			}
		}
	}
	
}
