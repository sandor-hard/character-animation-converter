package com.hardsoft.charanim.convert;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

public class FileSequenceAnimProvider extends AbstractDitheredSequenceAnimProvider {

	@Override
	protected MarvinImage generateFrameImage(String uri, int frame) {
		final String fileName = String.format(uri, frame+1);
        System.out.println(String.format("Processing frame %d: %s", frame, fileName));
		return MarvinImageIO.loadImage(fileName);
	}
	
}
