package com.hardsoft.charanim.convert;

public class CharAnimConverterMode7 extends AbstractCharAnimConverter {

	@Override
	protected int getCharHeight() {
		return 16;
	}

	@Override
	protected int getCharWidth() {
		return 16;
	}

	@Override
	protected int getBpp() {
		return 1;
	}

	@Override
	protected int getCharsPerCharset() {
		return 64;
	}
	
	@Override
	protected boolean getSupportsInverseChars() {
		return false;
	}
	
}
