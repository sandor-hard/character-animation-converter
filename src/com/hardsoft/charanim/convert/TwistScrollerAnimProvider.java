package com.hardsoft.charanim.convert;

import java.awt.Color;

import com.hardsoft.charanim.convert.twistscroller.TwistScrollerGenerator;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

public class TwistScrollerAnimProvider extends AbstractDitheredSequenceAnimProvider {

	private static final int FRAME_COUNT = 16;
	private final int[][][] mResult;
	private final Color[] mColors = new Color[0x100];
	
	public TwistScrollerAnimProvider() {
		super();
		
		mResult = TwistScrollerGenerator.renderFrames(FRAME_COUNT);
		
		for (int c=0;c<0x100;c++) {
			mColors[c] = new Color(c, c, c);
		}
	}
	
	@Override
	protected MarvinImage generateFrameImage(String uri, int frame) {
		if (frame<FRAME_COUNT) {
			final MarvinImage image = MarvinImageIO.loadImage("dontdelete/reference.jpg").clone();
			
			image.resize(OUT_RES_X, OUT_RES_Y);

			for (int bits=0;bits<0x20;bits++) {
				final int[] cBuffer = mResult[frame][bits];
				final int offsX = bits*8;
				for (int l=0;l<cBuffer.length;l++) {
					image.fillRect(offsX, l, 8, 1, mColors[cBuffer[l]]);
				}
			}
			
			image.update();
			return image.clone();
		}
		else {
			return null;
		}
	}
	
}
