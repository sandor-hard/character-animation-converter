/*
    Character animation converter
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to modify/use whatever way you like
 */

package com.hardsoft.charanim.convert;

import java.awt.Color;
import java.awt.Graphics;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class CharAnimConverter extends JPanel { 
	
	private static final long serialVersionUID = -4287093822991664351L;

	static class RawAnim {

		final Palette palette;
		final List<int[][]> frames = new ArrayList<int[][]>();
		final int[] frameIndices;

		public RawAnim(Palette palette, int frameCount) {
			this.palette = palette;
			frameIndices = new int[frameCount];
		}

	}

	static class CharAnim {

		final List<int[][][]> charSets = new ArrayList<int[][][]>();
		final int[] frameCharsets;
		final int[][][] frames;
		final int charWidth;
		final int charHeight;
		final int bpp;
		int[] frameIndices;
		
		public CharAnim(int frameCount, int width, int height, int charWidth, int charHeight, int bpp) {
			frameCharsets = new int[frameCount];
			frames = new int[frameCount][width][height];
			this.charWidth = charWidth;
			this.charHeight = charHeight;
			this.bpp = bpp;
		}
		

	}
	
	interface AnimProvider {
		RawAnim getRawAnim(String uri);
	}
	
	interface AnimConverter {
		CharAnim getCharAnim(RawAnim rawAnim, int transitionColorIndex, int exactColorIndex, boolean singleCharset);
	}
	
	static class Palette {

		final static int COLOR_INDEX_BK = 0;
		final static int COLOR_INDEX_PF0 = 1;
		final static int COLOR_INDEX_PF1 = 2;
		final static int COLOR_INDEX_PF2 = 3;
		final static int COLOR_INDEX_PF3 = 4;
		
		final int[] colors;

		public Palette(int colbk, int colpf0, int colpf1, int colpf2, int colpf3) {
			colors = new int[]{colbk, colpf0, colpf1, colpf2, colpf3};
		}
		
		
	}
	
	static class Memory {
		final byte[] mBytes = new byte[0x10000];
		int mCount = 0;
		
		public void write(byte b) {
			mBytes[mCount] = b;
			mCount++;
		}
	}
	
	static class AnimChangeSequence {
		final int change;
		final int changeRepeatCnt;
		final int[] changes;
		
		public AnimChangeSequence(int change, int changeRepeatCnt, int[] changes) {
			this.change = change;
			this.changeRepeatCnt = changeRepeatCnt;
			this.changes = changes;
		}
	}
	
	private CharAnim mCharAnim;
	private Palette mPalette;
	
	public static void main(String[] args) {
		System.out.println("HARD CharAnimConverter");

		//createReadyScreenAnimation();
		createTwisterScrollAnimation();
		//createFileSequenceAnimation();
	}
	
	private static void createReadyScreenAnimation() {
		final Memory memory = new Memory();
		final RawAnim rawAnim = new RotatingReadyScreenAnimProvider().getRawAnim(null);
		final CharAnim charAnim = new CharAnimConverterMode7().getCharAnim(
				rawAnim, Palette.COLOR_INDEX_PF0, Palette.COLOR_INDEX_PF1, true);
		writeCharsetToMemory(memory, charAnim);
		writeDeltaAnimationToMemory(memory, charAnim);
		save(memory, "inout/charanim.dat");
		preview(charAnim, rawAnim.palette);
	}

	private static void createFileSequenceAnimation() {
		final Memory memory = new Memory();
		final RawAnim rawAnim = new FileSequenceAnimProvider().getRawAnim("inout/%d.png");
		final CharAnim charAnim = new CharAnimConverterMode2().getCharAnim(rawAnim, -1, -1, true);
		writeCharsetToMemory(memory, charAnim);
		writeDeltaAnimationToMemory(memory, charAnim);
		save(memory, "inout/charanim.dat");
		preview(charAnim, rawAnim.palette);
	}
	
	private static void createTwisterScrollAnimation() {
		final Memory memory = new Memory();
		final RawAnim rawAnim = new TwistScrollerAnimProvider().getRawAnim(null);
		final CharAnim charAnim = new CharAnimConverterMode4().getCharAnim(rawAnim, -1, -1, true);
		writeCharsetToMemory(memory, charAnim);
		writeTwisterAnimationToMemory(memory, charAnim, 0x20, 0x08);
		save(memory, "inout/charanim.dat");
		preview(charAnim, rawAnim.palette);
	}
	
	private static void writeTwisterAnimationToMemory(Memory memory, CharAnim charAnim,
			int width, int height) {
		final int animLength = charAnim.frameIndices.length;
		for (int a=0;a<animLength;a++) {
			final int[][] frame = charAnim.frames[charAnim.frameIndices[a]];
			for (int x=0;x<width;x++) {
				for (int y=0;y<height;y++) {
					memory.write((byte)frame[x][y]);
				}
			}
		}
	}

	private static void writeDeltaAnimationToMemory(Memory memory, CharAnim charAnim) {
		final int animLength = charAnim.frameIndices.length;
		final int frameWidth = charAnim.frames[0].length;
		final int frameHeight = charAnim.frames[0][0].length;
		final int[][][] animChanges = new int[animLength][frameWidth][frameHeight];
		final boolean[] anyChangeInStep = new boolean[animLength];
		final int[] maxChangedRowInStep = new int[animLength];
		
		for (int a=0;a<animLength;a++) {
			final int[][] prevFrame = a>0?charAnim.frames[charAnim.frameIndices[a-1]]:null;
			final int[][] currFrame = charAnim.frames[charAnim.frameIndices[a]];
			
			for (int x=0;x<frameWidth;x++) {
				for (int y=0;y<frameHeight;y++) {
					final boolean isDifferent = prevFrame==null?true:prevFrame[x][y]!=currFrame[x][y];
					if (isDifferent) {
						animChanges[a][x][y] = currFrame[x][y];
						anyChangeInStep[a] = true;
						
						if (y>maxChangedRowInStep[a]) {
							maxChangedRowInStep[a] = y;
						}
					}
					else {
						animChanges[a][x][y] = -1;
					}
				}
			}
		}
		
		for (int a=0;a<animLength;a++) {
			final List<AnimChangeSequence> sequences = new ArrayList<AnimChangeSequence>();
			
			if (anyChangeInStep[a]) {
				final int[][] changes = animChanges[a];
				int lastChange = -1;
				int sameChangeInRow = 0;
				
				for (int y=0;y<=maxChangedRowInStep[a];y++) {
					for (int x=0;x<frameWidth;x++) {
						final int currChange = changes[x][y];
						if (currChange==lastChange) {
							sameChangeInRow++;
						}
						else {
							if (sameChangeInRow>0) {
								sequences.add(new AnimChangeSequence(lastChange, sameChangeInRow, null));
							}
							
							lastChange = currChange;
							sameChangeInRow = 1;
						}
					}
				}
	
				if (sameChangeInRow>0 && lastChange>=0) {
					sequences.add(new AnimChangeSequence(lastChange, sameChangeInRow, null));
				}
			}

			final List<AnimChangeSequence> optimizedSequences = new ArrayList<AnimChangeSequence>();
			final List<AnimChangeSequence> shortSequences = new ArrayList<AnimChangeSequence>();
			for (AnimChangeSequence sequence : sequences) {
				if (sequence.changeRepeatCnt<2 && sequence.change>=0) {
					//accumulate and combine into an unrolled segment
					shortSequences.add(sequence);
				}
				else {
					if (!shortSequences.isEmpty()) {
						unrollChanges(shortSequences, optimizedSequences);
					}
					
					optimizedSequences.add(sequence);
				}

				if (!shortSequences.isEmpty()) {
					unrollChanges(shortSequences, optimizedSequences);
				}
			}
			
			for (AnimChangeSequence sequence : optimizedSequences) {
				if (sequence.changeRepeatCnt>0) {
					//repeating values
					if (sequence.change>=0) {
						memory.write((byte)(sequence.changeRepeatCnt|0x80));
						memory.write((byte)sequence.change);
					}
					else {
						memory.write((byte)0xfd); //skip n bytes on screen
						memory.write((byte)(sequence.changeRepeatCnt));
					}
				}
				else {
					//varying values
					memory.write((byte)sequence.changes.length);
					for (int c=0;c<sequence.changes.length;c++) {
						memory.write((byte)sequence.changes[c]);
					}
				}
			}

			memory.write((byte)0xfe); //end of frame
		}
		
		memory.write((byte)0xff); //end of animation
	}

	private static void writeCharsetToMemory(final Memory memory, CharAnim charAnim) {
		for (int cs=0;cs<charAnim.charSets.size();cs++) {
			final int[][][] charSet = charAnim.charSets.get(cs);
			final int bpp = 8/charSet[0].length;
			
			for (int ch=0;ch<charSet.length;ch++) {
				final int[][] charData = charSet[ch];
				
				for (int pY=0;pY<charData[0].length;pY++) {
					int b = 0;
					for (int pX=0;pX<charData.length;pX++) {
						final int c = charData[pX][pY];
						if (bpp==1) {
							if (c>0) {
								b|= 1<<(7-pX);
							}
						}
						else if (bpp==2) {
							if (c>0 && c<4) {
								b|= c<<(6-pX*2);
							}
						}
						else {
							throw new RuntimeException("Unsupported BPP setting");
						}
					}
					
					memory.write((byte)b);
				}
			}
		}
	}

	private static void unrollChanges(List<AnimChangeSequence> shortSequences,
			List<AnimChangeSequence> optimizedSequences) {
		int cnt = 0;
		for (AnimChangeSequence sequence : shortSequences) {
			for (int c=0;c<sequence.changeRepeatCnt;c++) {
				cnt+= sequence.changeRepeatCnt;
			}
		}
		
		final int[] values = new int[cnt];
		int v = 0;
		for (AnimChangeSequence sequence : shortSequences) {
			for (int c=0;c<sequence.changeRepeatCnt;c++) {
				values[v] = sequence.change;
				v++;
			}
		}
		
		final AnimChangeSequence combinedSequence = new AnimChangeSequence(-1, -1, values);
		optimizedSequences.add(combinedSequence);
		shortSequences.clear();
	}

	private static void save(Memory memory, String fileName) {
		FileOutputStream f = null;
		try {
			final File file = new File(fileName);
			file.delete();
			f = new FileOutputStream(file);
			f.write(memory.mBytes, 0, memory.mCount);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (f!=null) {
				try {
					f.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public CharAnimConverter(CharAnim charAnim, Palette palette) {
		mCharAnim = charAnim;
		mPalette = palette;
	}
	
	public void paintComponent(Graphics g) {

		final Color[] colors = new Color[16];
		for (int c=0;c<16;c++) {
			final int b = (c>>1)*32;
			colors[c] = new Color(b, b, b);
		}

		final int xRatioFactor = 4;
		final int yRatioFactor = 4;
		
		final int animLength = mCharAnim.frameIndices.length;
		
		final int itemsPerPseudoCell = animLength/(xRatioFactor*yRatioFactor);
		int itemsX = (int) (Math.ceil(Math.sqrt(itemsPerPseudoCell))*xRatioFactor);
		int itemsY = (int) (Math.ceil(Math.sqrt(itemsPerPseudoCell))*yRatioFactor);
		
		if (itemsX==0) {
			itemsX = 4;
		}
		
		if (itemsY==0) {
			itemsY = 4;
		}
		
		final double itemScale = (double) getWidth() / (double) itemsX / mCharAnim.frames[0].length / (float)mCharAnim.charWidth * 8f;

		final int linesPerCharLine = mCharAnim.charHeight/8;
		final int pixelsPerCharPixel = mCharAnim.charWidth/8;
		final double charWidthInPreviewPixels = (double) pixelsPerCharPixel * itemScale;
		final double charHeightInPreviewPixels = (double) linesPerCharLine * itemScale;
		
		System.out.println(String.format("Preview grid size: %dx%d = %d items (frame count = %d)", itemsX, itemsY, itemsX*itemsY, animLength));
		
		for (int iX=0;iX<itemsX;iX++) {
			for (int iY=0;iY<itemsX;iY++) {
				final int f = iY*itemsX+iX;
				if (f<animLength) {
					final int[][][] charSet = mCharAnim.charSets.get(mCharAnim.frameCharsets[mCharAnim.frameIndices[f]]);
					final int[][] frame = mCharAnim.frames[mCharAnim.frameIndices[f]];

					final double offsX = (double) iX * (double) frame.length * (double) pixelsPerCharPixel * itemScale;
					final double offsY = (double) iY * (double) frame[0].length * (double) linesPerCharLine * itemScale;
					
					for (int cX=0;cX<frame.length;cX++) {
						for (int cY=0;cY<frame[0].length;cY++) {
							final int charIndex = frame[cX][cY];
							final boolean isInverse = charIndex>=0x80;
							final int[][] charData = charSet[charIndex&0x7f];
							
							for (int pX=0;pX<charData.length;pX++) {
								final double xProgress = pX / (double)charData.length;
								for (int pY=0;pY<charData[0].length;pY++) {
									final int maxC = (1<<mCharAnim.bpp)-1;
									final int colorIndex = isInverse?maxC-charData[pX][pY]:charData[pX][pY];
									
									final int c = (mPalette.colors[colorIndex])&0xf;
									
									final double yProgress = pY / (double)charData[0].length;
									
									final int x = (int) Math.round(offsX + charWidthInPreviewPixels*(cX+xProgress));
									final int y = (int) Math.round(offsY + charHeightInPreviewPixels*(cY+yProgress));

									g.setColor(colors[c]);
									g.fillRect(x, y, (int)charWidthInPreviewPixels, (int)charHeightInPreviewPixels);
								}
							}
						}
					}
					
					g.setColor(colors[8]);
					final double endX = (double) (iX+1) * (double) frame.length * (double) pixelsPerCharPixel * itemScale - 1f;
					final double endY = (double) (iY+1) * (double) frame[0].length * (double) linesPerCharLine * itemScale - 1f;
					g.drawLine((int) offsX, (int) offsY, (int) endX, (int) offsY);
					g.drawLine((int) endX, (int) offsY, (int) endX, (int) endY);
					g.drawLine((int) endX, (int) endY, (int) offsX, (int) endY);
					g.drawLine((int) offsX, (int) endY, (int) offsX, (int) offsY);
				}
			}
		}
		
	}
	
//	private int readMem(int addr) {
//		final byte b = mMemory.mBytes[addr];
//		return b>=0?(int)b:((int)b)+0x100;
//	}

	private static void preview(CharAnim charAnim, Palette palette) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("CharAnimConverter Preview");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.black);
		
		frame.setSize(1024, 640);

		CharAnimConverter panel = new CharAnimConverter(charAnim, palette);

		frame.add(panel);

		frame.setVisible(true);
	}
	
}