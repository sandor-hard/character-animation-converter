/*
    Character animation converter
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to modify/use whatever way you like
 */

package com.hardsoft.charanim.convert;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.hardsoft.charanim.convert.CharAnimConverter.AnimConverter;
import com.hardsoft.charanim.convert.CharAnimConverter.CharAnim;
import com.hardsoft.charanim.convert.CharAnimConverter.Palette;
import com.hardsoft.charanim.convert.CharAnimConverter.RawAnim;

public abstract class AbstractCharAnimConverter implements AnimConverter {

	class CharDataUsage implements Comparable {
		final String checksum;
		final int cnt;
		String replacementChecksum;
		
		public CharDataUsage(String checksum, int cnt) {
			this.checksum = checksum;
			this.cnt = cnt;
		}
		@Override
		public int compareTo(Object o) {
			return Integer.compare(((CharDataUsage) o).cnt, cnt);
		}
	}
	
	@Override
	public CharAnim getCharAnim(RawAnim rawAnim, int transitionColorIndex, int exactColorIndex, boolean singleCharset) {
		final int charHeight = getCharHeight();
		final int charWidth = getCharWidth();
		final int bpp = getBpp();
		final int linesPerCharLine = charHeight/8;
		final int pixelsPerCharPixel = charWidth/8;

		final int charsPerCharset = getCharsPerCharset();
		final boolean supportsInverseChars = getSupportsInverseChars();
		
		final Map<String, int[][]> charDataMap = new HashMap<String, int[][]>();
		final Map<String, BigInteger> charDataBigIntegerMap = new HashMap<String, BigInteger>();
		final Map<String, Integer> checksumCntMap = new HashMap<String, Integer>();
		final Map<String, Boolean> checksumExactColorMap = new HashMap<String, Boolean>();
		final int width = rawAnim.frames.get(0).length;
		final int height = rawAnim.frames.get(0)[0].length;
		final String[][][] frameChecksums = new String[rawAnim.frames.size()][width/charWidth][height/charHeight];
		final Map<String, Boolean> isInvChecksumMap = new HashMap<String, Boolean>();
		final Map<String, String> checksumToInvChecksumMap = new HashMap<String, String>();
		
		final CharAnim result = new CharAnim(rawAnim.frames.size(), width/charWidth, height/charHeight, charWidth, charHeight, bpp);
		result.frameIndices = rawAnim.frameIndices;

		for (int f=0;f<rawAnim.frames.size();f++) {
			final int[][] frame = rawAnim.frames.get(f);

			for (int cX=0;cX<width;cX+=charWidth) {
				for (int cY=0;cY<height;cY+=charHeight) {
					
					final int charX = cX/charWidth;
					final int charY = cY/charHeight;
					
					int[][] charData = new int[8/bpp][8];
					boolean didUseExactColor = false;					
					
					for (int pX=0;pX<charWidth;pX+=(pixelsPerCharPixel*bpp)) {
						for (int pY=0;pY<charHeight;pY+=linesPerCharLine) {
							int c = frame[cX+pX][cY+pY];
							
							if (bpp==2) {
								final int c2 = frame[cX+pX+1][cY+pY];
								c = resolveColor(c, c2, transitionColorIndex);
							}
							
							if (c==exactColorIndex) {
								didUseExactColor = true;
							}
							
							charData[pX/pixelsPerCharPixel/bpp][pY/linesPerCharLine] = c;
						}
					}
					
					final String checksum = getChecksum(charData, bpp, false);
					final String invChecksum = supportsInverseChars?getChecksum(charData, bpp, true):null;
					final boolean existingChecksum = charDataMap.containsKey(checksum);
					final boolean existingInvChecksum = existingChecksum?false:(supportsInverseChars?charDataMap.containsKey(invChecksum):false);
					final String checksumToUse = existingInvChecksum?invChecksum:checksum;
					if (existingChecksum || existingInvChecksum) {
						charData = charDataMap.get(checksumToUse);
						checksumCntMap.put(checksumToUse, checksumCntMap.get(checksumToUse)+1);
					}
					else {
						charDataMap.put(checksum, charData);
						checksumCntMap.put(checksum, 1);
						checksumExactColorMap.put(checksum, didUseExactColor);

						final StringBuilder sb = new StringBuilder(0x40);
						final StringBuilder sbInv = new StringBuilder(0x40);
						
						for (int y=0;y<8;y++) {
							for (int x=0;x<8/bpp;x++) {
								if (bpp==2) {
									switch (charData[x][y]) {
									case 1:
										sb.append("01");
										sbInv.append("10");
										break;
									case 2:
										sb.append("10");
										sbInv.append("01");
										break;
									case 3:
										sb.append("11");
										sbInv.append("00");
										break;
									default: 
										sb.append("00");
										sbInv.append("11");
									}
								}
								else if (bpp==1) {
									sb.append(charData[x][y]>0?"1":"0");
									sbInv.append(charData[x][y]>0?"0":"1");
								}
								else {
									throw new RuntimeException("Unsupported BPP setting");
								}
							}
						}
						
						final String binData = sb.toString();
						final BigInteger bigInt = new BigInteger(binData, 2);
						charDataBigIntegerMap.put(checksum, bigInt);

						if (supportsInverseChars) {
							final String binDataInv = sbInv.toString();
							final BigInteger bigIntInv = new BigInteger(binDataInv, 2);
							charDataBigIntegerMap.put(invChecksum, bigIntInv);
						}
					}
					
					frameChecksums[f][charX][charY] = checksum;
					if (supportsInverseChars) {
						checksumToInvChecksumMap.put(checksum, invChecksum);
						checksumToInvChecksumMap.put(invChecksum, checksum);
						
						if (existingInvChecksum) {
							isInvChecksumMap.put(invChecksum, false);
							isInvChecksumMap.put(checksum, true);
						}
						else {
							isInvChecksumMap.put(checksum, false);
							isInvChecksumMap.put(invChecksum, true);
						}
					}
				}
			}
		}

		System.out.println(String.format("Number of unique chars in animation: %d", charDataMap.size()));
		
		final Map<String, Integer> charIndices = new HashMap<String, Integer>();

		if (singleCharset) {
			final Set<String> checksums = checksumCntMap.keySet();
			final CharDataUsage[] stats = new CharDataUsage[checksums.size()];
			int chix = 0;
			for (String checksum : checksums) {
				final int cnt = checksumCntMap.get(checksum);
				final boolean didUseExactColor = checksumExactColorMap.get(checksum);
				final CharDataUsage u = new CharDataUsage(checksum, didUseExactColor?Integer.MAX_VALUE:cnt);
				stats[chix] = u;
				chix++;
			}

			Arrays.sort(stats);
			
			final Map<String, String> replacementMap = new HashMap<String, String>();

			if (stats.length>charsPerCharset) {
				for (int s=0;s<Math.min(charsPerCharset, stats.length);s++) {
					final CharDataUsage u = stats[s];
					charIndices.put(u.checksum, s);
					System.out.println(String.format("Top #%d char: %s, cnt=%d", s, u.checksum, u.cnt));					
				}
				
				for (int s=charsPerCharset;s<stats.length;s++) {
					final CharDataUsage u = stats[s];
					replacementMap.put(u.checksum, getNearestMatch(stats, charDataBigIntegerMap, checksumToInvChecksumMap, u));
				}
			}
			
			for (int f=0;f<frameChecksums.length;f++) {
				for (int x=0;x<frameChecksums[f].length;x++) {
					for (int y=0;y<frameChecksums[f][x].length;y++) {
						String replacementChecksum = null;
						final boolean isInverse = supportsInverseChars&&isInvChecksumMap.get(frameChecksums[f][x][y]);
						if (isInverse) {
							final String checksum = checksumToInvChecksumMap.get(frameChecksums[f][x][y]);
							final String replacementChecksumOrig = replacementMap.get(checksum);
							replacementChecksum = checksumToInvChecksumMap.get(replacementChecksumOrig);
						}
						else {
							replacementChecksum = replacementMap.get(frameChecksums[f][x][y]);
						}
						
						if (replacementChecksum!=null) {
							frameChecksums[f][x][y] = replacementChecksum;
						}
					}
				}
			}
		}
		else {
			throw new RuntimeException("Multi-charset mode is not supported yet");
		}
		
		//TODO: Support multi-charset
		final int[][][] charset = new int[charsPerCharset][][];
		for (int f=0;f<frameChecksums.length;f++) {
			result.frameCharsets[f] = 0;
			for (int x=0;x<frameChecksums[f].length;x++) {
				for (int y=0;y<frameChecksums[f][x].length;y++) {
					final String rawChecksum = frameChecksums[f][x][y];
					final boolean isInverse = supportsInverseChars&&isInvChecksumMap.get(rawChecksum);
					final String checksum = isInverse?checksumToInvChecksumMap.get(rawChecksum):rawChecksum;
					final int[][] charData = charDataMap.get(checksum);
					final int charIndex = charIndices.get(checksum);
					charset[charIndex] = charData;
					result.frames[f][x][y] = charIndex+(isInverse?0x80:0);
				}
			}
		}
		result.charSets.add(charset);
	
		return result;
	}

	private String getNearestMatch(CharDataUsage[] stats, Map<String, BigInteger> charDataBigIntegerMap,
			Map<String, String> checksumToInvChecksumMap, CharDataUsage uRef) {
		String result = null;
		
		final BigInteger bigIntRef = charDataBigIntegerMap.get(uRef.checksum);
		int minCnt = Integer.MAX_VALUE;
		
		for (int s=0;s<getCharsPerCharset();s++) {
			final CharDataUsage u = stats[s];
			final BigInteger diff = charDataBigIntegerMap.get(u.checksum).xor(bigIntRef);
			final int diffCnt = diff.bitCount();
			
			if (diffCnt<minCnt) {
				minCnt = diffCnt;
				result = u.checksum;
			}
			
			if (getSupportsInverseChars()) {
				final String invChecksum = checksumToInvChecksumMap.get(u.checksum);
				final BigInteger diffInv = charDataBigIntegerMap.get(invChecksum).xor(bigIntRef);
				final int diffCntInv = diffInv.bitCount();
				
				if (diffCntInv<minCnt) {
					minCnt = diffCntInv;
					result = invChecksum;
				}
			}
		}

		System.out.println(String.format("Replacing %s with %s (diff: %d)", uRef.checksum, result, minCnt));
		
		return result;
	}

	protected abstract int getCharHeight();
	protected abstract int getCharWidth();
	protected abstract int getBpp();
	protected abstract int getCharsPerCharset();
	protected abstract boolean getSupportsInverseChars();
	
	private String getChecksum(int[][] arr, int bpp, boolean inv) {
		String result = null;
		
		final int cnt = arr.length * arr[0].length;
		final byte[] bytes = new byte[cnt];
		
		int bx = 0;
		for (int x=0;x<arr.length;x++) {
			for (int y=0;y<arr[0].length;y++) {
				final int maxC = (1<<bpp)-1;
				final int c = inv?maxC-arr[x][y]:arr[x][y];
				bytes[bx] = (byte) c;
				bx++;
			}
		}
				
        final MessageDigest m;
        try {
            m = MessageDigest.getInstance("SHA1");
            m.update(bytes);
            result = String.format("%x", new BigInteger(1, m.digest()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("Can not calculate checksum");
        }
        
        return result;
	}
	
	private int resolveColor(int c1, int c2, int c3) {
		return ((c1==c2) || c1==Palette.COLOR_INDEX_BK || c2==Palette.COLOR_INDEX_BK)?c1:c3;
	}

}
