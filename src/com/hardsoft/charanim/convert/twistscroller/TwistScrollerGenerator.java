/*
    Twister Scroller animation generator
    used in the production of the REHARDEN demo (Atari XL/XE)

    Created by Sandor Teli / HARD (10/2016-11/2017)

    Feel free to modify/use whatever way you like
 */

package com.hardsoft.charanim.convert.twistscroller;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.hardsoft.charanim.convert.twistscroller.VectorHelper.Vector3d;

public class TwistScrollerGenerator { 
	
	private static final int DISPLAY_HEIGHT_IN_PIXELS = 8*8;
	private static final int TEXT_BIT_COUNT = 5;
	private static double TEXT_BORDER_RATIO = 0.1875d;
	
	private static final double mEyeY = 0f;
	private static final double mEyeZ = -7f;
	
	private static final double mLightVectorY = 0f;
	private static final double mLightVectorZ = -1f;
	private static final double mLightPosY = -2f;
	private static final double mLightPosZ = 4f;
	private static final double mLightMaxStrenghtDistance = 5f;
	
	private static final double mDotProductWeight = 0.75f;
	
	private static final int mMeshWidth = 8;
	private static final int mMeshHeight = 8;
	private static final int mMeshResolution = 32;

	private static final double TEXT_INTENSITY = 0.2d;
	private static final double ROTATION_RANGE = Math.PI * 0.5d;

	private static int frameCount;
	
	private static class Vertex {
		private final double mY;
		private final double mZ;
		private final double mIntensity;

		public Vertex(double y, double z, double intensity) {
			mY = y;
			mZ = z;
			mIntensity = intensity;
		}
	}
	
	private static class Face {
		private final Vertex mV1;
		private final Vertex mV2;
		private final Vertex mOrigV1;
		private final Vertex mOrigV2;
		private final Vertex mMeshV1;
		private final Vertex mMeshV2;
		private final Vector3d mNormal;
		private final double mDotProduct;
		private final double mIntensity;

		public Face(Vertex v1, Vertex v2, Vertex origV1, Vertex origV2, Vertex meshV1, Vertex meshV2) {
			mV1 = v1;
			mV2 = v2;
			mOrigV1 = origV1;
			mOrigV2 = origV2;
			mMeshV1 = meshV1;
			mMeshV2 = meshV2;
			mNormal = VectorHelper.getFaceNormal(origV1.mY, origV1.mZ, origV2.mY, origV2.mZ);
			mDotProduct = VectorHelper.getDotProduct(mNormal.y, mNormal.z, mLightVectorY, mLightVectorZ);
			final double faceMiddleY = (origV1.mY+origV2.mY)/2f;
			final double faceMiddleZ = (origV1.mZ+origV2.mZ)/2f;
			final double ld = Math.hypot(mLightPosY-faceMiddleY, mLightPosZ-faceMiddleZ);
			final double lr = ld>mLightMaxStrenghtDistance?0f:(mLightMaxStrenghtDistance-ld)/mLightMaxStrenghtDistance;
			final double intensity = 1-Math.abs(mDotProduct);
			final double dotFactor = intensity*intensity*intensity*intensity*intensity*intensity;
			final double lightFactor = lr*lr*lr*lr*lr*lr*lr*lr;
			//mIntensity = mMeshV1.mIntensity * ((1-mDotProductWeight)+iPow*mDotProductWeight) * (mDotProductWeight+(lrPow*(1-mDotProductWeight)));
			final double iScale = Math.min(dotFactor + lightFactor, 1d);
			mIntensity = mMeshV1.mIntensity * iScale;
		}
	}
	
	private static class RenderResult {

		private final int numColorLines;
		private final double[] iBuffer;
		private final double minIntensity;
		private final double maxIntensity;

		public RenderResult(int numColorLines, double[] iBuffer,
				double minIntensity, double maxIntensity) {
			this.numColorLines = numColorLines;
			this.iBuffer = iBuffer;
			this.minIntensity = minIntensity;
			this.maxIntensity = maxIntensity;
		}

	}

	private static class Frame {
		private final double mMinY;
		private final double mMaxY;
		private final double mHeight;
		private final Vertex[] mVertices;
		private final Vertex[] mOrigVertices;
		private final Vertex[] mMeshVertices;
		private final Face[] mFaces;
		
		private int mStartColorLine;
		private int mEndColorLine;
		private RenderResult mRenderResult;

		private int mStartPixelLine;
		private int mEndPixelLine;
		
		public Frame(double minY, double maxY, Vertex[] vertices, Vertex[] origVertices, Vertex[] meshVertices) {
			mMinY = minY;
			mMaxY = maxY;
			mHeight = maxY - minY;
			mVertices = vertices;
			mOrigVertices = origVertices;
			mMeshVertices = meshVertices;
			mFaces = new Face[vertices.length];
			initFaces();
		}
		
		private void initFaces() {
			for (int v=0;v<mVertices.length;v++) {
				final Vertex v1 = mVertices[v];
				final Vertex v2 = mVertices[v==(mVertices.length-1)?0:v+1];
				final Vertex origV1 = mOrigVertices[v];
				final Vertex origV2 = mOrigVertices[v==(mVertices.length-1)?0:v+1];
				final Vertex meshV1 = mMeshVertices[v];
				final Vertex meshV2 = mMeshVertices[v==(mMeshVertices.length-1)?0:v+1];
				mFaces[v] = new Face(v1, v2, origV1, origV2, meshV1, meshV2);
			}
		}

		public void render(int frame, double pixelsPerUnit, double displayHeight) {
			final double offset = displayHeight/2f;
			
			final double[] zBuffer = new double[DISPLAY_HEIGHT_IN_PIXELS];
			final double[] iBuffer = new double[DISPLAY_HEIGHT_IN_PIXELS];
			
			for (int l=0;l<DISPLAY_HEIGHT_IN_PIXELS;l++) {
				zBuffer[l] = Double.NEGATIVE_INFINITY;
				iBuffer[l] = 0f;
			}
			
			mStartColorLine = Integer.MAX_VALUE;
			mEndColorLine = Integer.MIN_VALUE;
			mStartPixelLine = Integer.MAX_VALUE;
			mEndPixelLine = Integer.MIN_VALUE;
			
			double minIntensity = Double.MAX_VALUE;
			double maxIntensity = Double.NEGATIVE_INFINITY;
			
			for (int f=0;f<mFaces.length;f++) {
				final Face face = mFaces[f];
				if (true/*face.mNormal.z<0f*/) {
					final double z = Math.max(face.mOrigV1.mZ, face.mOrigV2.mZ);
					final double startY = Math.min(face.mV1.mY, face.mV2.mY);
					final double endY = Math.max(face.mV1.mY, face.mV2.mY);
					int startColorLine = (int) Math.round((offset + startY) * pixelsPerUnit);
					int endColorLine = (int) Math.round((offset + endY) * pixelsPerUnit);
					
					if (startColorLine<0) {
						startColorLine = 0;
					}
					if (endColorLine>=DISPLAY_HEIGHT_IN_PIXELS) {
						endColorLine = DISPLAY_HEIGHT_IN_PIXELS-1;
					}
					
					if (mStartColorLine>startColorLine) {
						mStartColorLine = startColorLine;
					}
					if (mEndColorLine<endColorLine) {
						mEndColorLine = endColorLine;
					}
					
					for (int l=startColorLine;l<=endColorLine;l++) {
						if (zBuffer[l]<z) {
							zBuffer[l] = z;
							iBuffer[l] = face.mIntensity;
						}
					}
					
					int startPixelLine = (int) Math.round((offset + startY) * pixelsPerUnit);
					int endPixelLine = (int) Math.round((offset + endY) * pixelsPerUnit);
					
					if (startPixelLine<0) {
						startPixelLine = 0;
					}
					if (endPixelLine>=DISPLAY_HEIGHT_IN_PIXELS) {
						endPixelLine = DISPLAY_HEIGHT_IN_PIXELS-1;
					}
					
					if (mStartPixelLine>startPixelLine) {
						mStartPixelLine = startPixelLine;
					}
					if (mEndPixelLine<endPixelLine) {
						mEndPixelLine = endPixelLine;
					}
					
				}
			}

			for (int l=mStartColorLine;l<=mEndColorLine;l++) {
				if (iBuffer[l]>0f) {
					if (maxIntensity<iBuffer[l]) {
						maxIntensity = iBuffer[l];
					}
					if (minIntensity>iBuffer[l]) {
						minIntensity = iBuffer[l];
					}
				}
			}
			
			final int numColorLines = mEndColorLine - mStartColorLine + 1;

			mRenderResult = new RenderResult(numColorLines, iBuffer, minIntensity, maxIntensity);
		}
		
		public int[] render(double minIntensity, double maxIntensity) {
			final int[] cBufferOut = new int[DISPLAY_HEIGHT_IN_PIXELS];
			for (int l=mStartColorLine;l<=mEndColorLine;l++) {
				final double intensity = mRenderResult.iBuffer[l];
				final double range = maxIntensity - minIntensity;
				final int color = intensity==0f?0:(int) Math.round(254f/range*(intensity-minIntensity));
				
				cBufferOut[l] = color;
			}

			return cBufferOut;
		}
		
	}
	
	static ArrayList<Vertex> mVertices = new ArrayList<Vertex>();
	
	public static int[][][] renderFrames(int frameCnt) {
		System.out.println("RRGen");
		
		final int[][][] result = new int[frameCnt][0x20][DISPLAY_HEIGHT_IN_PIXELS];
		
		frameCount = frameCnt;
		
		final double rotateStep = ROTATION_RANGE/(double) frameCount;
		final double rotationOffset = 0;
		
		final Frame[][] frames = new Frame[frameCount][0x20];
		
		double minY = Double.MAX_VALUE;
		double maxY = Double.NEGATIVE_INFINITY;
		double minIntensity = Double.MAX_VALUE;
		double maxIntensity = Double.NEGATIVE_INFINITY;
		
		for (int f=0;f<frameCount;f++) {
			System.out.println(String.format("\nframe: %d", f));
						
			final double rotation = rotationOffset + rotateStep * (double) f;

			for (int bits=0;bits<0x20;bits++) {
				initMesh(bits);
				
				double frameMinY = Double.MAX_VALUE;
				double frameMaxY = Double.NEGATIVE_INFINITY;
				
				Vertex[] frameVertices = new Vertex[mVertices.size()];
				Vertex[] origFrameVertices = new Vertex[mVertices.size()];
				Vertex[] meshVertices = new Vertex[mVertices.size()];
				
				for (int v=0;v<mVertices.size();v++) {
					meshVertices[v] = mVertices.get(v);
					
					final Vertex vRot = rotate(mVertices.get(v), rotation);
					final Vertex vPers = vRot;//persp(vRot);
					System.out.println(String.format("y:%f (z:%f)", vPers.mY, vPers.mZ));
					
					if (vPers.mY<frameMinY) {
						frameMinY = vPers.mY;
					}
					if (vPers.mY>frameMaxY) {
						frameMaxY = vPers.mY;
					}
					
					frameVertices[v] = vPers;
					origFrameVertices[v] = vRot;
				}
	
				if (frameMinY<minY) {
					minY = frameMinY;
				}
				if (frameMaxY>maxY) {
					maxY = frameMaxY;
				}
				
				frames[f][bits] = new Frame(frameMinY, frameMaxY, frameVertices, origFrameVertices, meshVertices);
				
				System.out.println(String.format("frameMinY:%f frameMaxY:%f, frameHeight:%f\n", frameMinY, frameMaxY, frames[f][bits].mHeight));
				
				for (int v=0;v<mVertices.size();v++) {
					final Face face = frames[f][bits].mFaces[v];
					//System.out.println(String.format("face v1.y:%f v2.y:%f (normal:%f, %f, %f; dot:%f, intensity:%f)", face.mV1.mY, face.mV2.mY, face.mNormal.x, face.mNormal.y, face.mNormal.z, face.mDotProduct, face.mIntensity));
				}
			}
		}
		
		final double height = maxY - minY;
		final double pixelsPerUnit = (double)(DISPLAY_HEIGHT_IN_PIXELS-1)/height;
		System.out.println(String.format("\nOverall minY:%f maxY:%f, height:%f, pixelsPerUnit:%f", minY, maxY, height, pixelsPerUnit));
		
		for (int f=0;f<frameCount;f++) {
			for (int bits=0;bits<0x20;bits++) {
				System.out.println(String.format("\nRendering frame: %d", f));
				frames[f][bits].render(f, pixelsPerUnit, height);
				if (maxIntensity<frames[f][bits].mRenderResult.maxIntensity) {
					maxIntensity = frames[f][bits].mRenderResult.maxIntensity;
				}
				if (minIntensity>frames[f][bits].mRenderResult.minIntensity) {
					minIntensity = frames[f][bits].mRenderResult.minIntensity;
				}
			}
		}

		for (int f=0;f<frameCount;f++) {
			for (int bits=0;bits<0x20;bits++) {
				System.out.println(String.format("\nRendering frame: %d", f));
				final int[] cBuffer = frames[f][bits].render(minIntensity, maxIntensity);
				result[f][bits] = cBuffer;
			}
		}
		
		return result;
	}

	private static void initMesh(int bits) {
		mVertices.clear();
		final double unit = 1f/(double)mMeshResolution;
		
		final double h = (double)(mMeshHeight*mMeshResolution);
		final double w = (double)(mMeshWidth*mMeshResolution);
		final double hBorder = TEXT_BORDER_RATIO * h;
		final double wBorder = TEXT_BORDER_RATIO * w;
		final double bitH = (h - 2d*hBorder)/(double)TEXT_BIT_COUNT;
		final double bitW = (w - 2d*wBorder)/(double)TEXT_BIT_COUNT;
		
		for (int y=0;y<mMeshHeight*mMeshResolution;y++) {
			final double vY = (double)-mMeshHeight/2f + unit*(double)y;
			final double vZ = (double)mMeshWidth/2f;
			
			final double dY = (double) y;
			double i = 1f;
			
			if (dY>=hBorder && dY<=(h-hBorder)) {
				final int bit = (int) Math.floor((dY-hBorder)/bitH);
				final boolean isBitSet = (bits&(1<<bit))!=0;
				i = isBitSet?TEXT_INTENSITY:1d;
			}
			
			mVertices.add(new Vertex(vY, vZ, i));
		}

		for (int z=0;z<mMeshWidth*mMeshResolution;z++) {
			final double vZ = (double)mMeshWidth/2f - unit*(double)z;
			final double vY = (double)mMeshHeight/2f;

			final double dZ = (double) z;
			double i = 1f;
			
			if (dZ>=wBorder && dZ<=(w-wBorder)) {
				final int bit = (int) Math.floor((dZ-wBorder)/bitW);
				final boolean isBitSet = (bits&(1<<bit))!=0;
				i = isBitSet?TEXT_INTENSITY:1d;
			}
			
			mVertices.add(new Vertex(vY, vZ, i));
		}
		
		for (int y=0;y<mMeshHeight*mMeshResolution;y++) {
			final double vY = (double)mMeshHeight/2f - unit*(double)y;
			final double vZ = (double)-mMeshWidth/2f;

			final double dY = h-(double) y;
			double i = TEXT_INTENSITY;
			
			if (dY>=hBorder && dY<=(h-hBorder)) {
				final int bit = (int) Math.floor((dY-hBorder)/bitH);
				final boolean isBitSet = (bits&(1<<bit))!=0;
				i = isBitSet?1d:TEXT_INTENSITY;
			}
			
			mVertices.add(new Vertex(vY, vZ, i));
		}

		for (int z=0;z<mMeshWidth*mMeshResolution;z++) {
			final double vZ = (double)-mMeshWidth/2f + unit*(double)z;
			final double vY = (double)-mMeshHeight/2f;
			mVertices.add(new Vertex(vY, vZ, 1f));
		}
	}

	private static Vertex persp(Vertex v) {
		final double y = (mEyeZ * (v.mY-mEyeY)) / (mEyeZ + v.mZ) + mEyeY;
		return new Vertex(y, v.mZ, v.mIntensity);
	}

	private static Vertex rotate(Vertex v, double rotation) {
		final double cos = Math.cos(rotation);
		final double sin = Math.sin(rotation);
		final double rY = cos * v.mY - sin * v.mZ;
		final double rZ = sin * v.mY + cos * v.mZ;		
		
		return new Vertex(rY, rZ, v.mIntensity);
	}

//	private int readMem(int addr) {
//		final byte b = mMemory.mBytes[addr];
//		return b>=0?(int)b:((int)b)+0x100;
//	}

}