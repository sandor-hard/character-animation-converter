package com.hardsoft.charanim.convert.twistscroller;

public class VectorHelper {

	public static class Vector3d {
		double x;
		double y;
		double z;
	}

	private static Vector3d offset(Vector3d v, Vector3d vOffset) {
		final Vector3d result = new Vector3d();
		result.x = v.x - vOffset.x;
		result.y = v.y - vOffset.y;
		result.z = v.z - vOffset.z;
		return result;
	}

	private static Vector3d getNormal(Vector3d v1, Vector3d v2) {
		final Vector3d result = new Vector3d();
		result.x = v1.y * v2.z - v1.z * v2.y;
		result.y = v1.z * v2.x - v1.x * v2.z;
		result.z = v1.x * v2.y - v1.y * v2.x;
		return result;
	}

	private static Vector3d normalize(Vector3d v) {
		final Vector3d result = new Vector3d();
		final double len = (double) (Math.sqrt(v.y*v.y + v.z*v.z));
		if (len!=0f) {
			result.x = v.x / len;
			result.y = v.y / len;
			result.z = v.z / len;
		}
		else {
			result.x = 0f;
			result.y = 0f;
			result.z = 0f;
		}
		return result;
	}

	public static Vector3d getFaceNormal(double y1, double z1, double y2, double z2) {
		final Vector3d p1 = new Vector3d();
		p1.x = 0f;
		p1.y = y1;
		p1.z = z1;
		final Vector3d p2 = new Vector3d();
		p2.x = 0f;
		p2.y = y2;
		p2.z = z2;
		final Vector3d p3 = new Vector3d();
		p3.x = 1f; //faking p3 out of p1 by altering x
		p3.y = p1.y;
		p3.z = p1.z;
		final Vector3d v1 = offset(p3, p2);
		final Vector3d v2 = offset(p1, p2);
		final Vector3d pn = getNormal(v1, v2);
		return normalize(pn);
	}
	
	public static double getDotProduct(double y1, double z1, double y2, double z2) {
		return Math.atan2(y1*z2 - z1*y2, y1*y2 + z1*z2) / Math.PI;
	}
}
